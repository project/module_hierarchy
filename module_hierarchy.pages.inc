<?php
/**
 * @file module_hierarchy.module
 * Output an admin page showing a hierarchical list of modules.
 */

/**
 * Menu callback.
 *
 * Generate a module hierarchy either from root or the given module.
 *
 * To get a subtree, use the path to the tree. For example:
 *  '/forum/comment' will show you comment module placed below forum module.
 * This will come in to the menu system as an array of path arguments.
 */
function module_hierarchy_page() {
  // Get our path arguments.
  $module_path = func_get_args();
  //dsm($module_path);
  
  // @todo: replace this with a function that doesn't hit the DB
  $modules_by_name = module_rebuild_cache();
  //dsm($modules_by_name);
  
  // Build the module tree.
  foreach ($modules_by_name as $name => $module) {
    // Modules that don't depend on anything go at the root.
    if (!count($module->info['dependents'])) {
      $module_tree[$name] = $module;
    }
    // Add references to the module objects to a children array so we have
    // a tree structure.
    foreach ($module->info['dependencies'] as $dependency) {
      $module->children[$dependency] = $modules_by_name[$dependency];
    }
  }
  //dsm($module_tree);
  
  // Get the subtree module if there is one.
  if (count($module_path)) {
    $base_module = end($module_path);
  }
  
  if (isset($base_module)) {
    // The path we pass to the theme function needs the current module removing.
    // Make a copy so we don't affect the breadcrumb.
    $list_path = $module_path;
    array_pop($list_path);
    
    $subtree = array($base_module => $modules_by_name[$base_module]);
    //dsm($list_path);
    $output = theme('module_hierarchy_list', $subtree, $list_path);
  }
  else {
    //dsm($module_tree);
    $output = theme('module_hierarchy_list', $module_tree);
  }
  
  // Create the breadcrumb.
  // This is all deeply deeply horrible code and either it's too soon after breakfast
  // or Drupal core really needs some helper functions for making breadcrumbs.
  if (count($module_path)) {
    $breadcrumb = drupal_get_breadcrumb();
    $base_path = 'admin/build/modules/hierarchy';
    
    $breadcrumb[] = l(t('Hierarchy'), $base_path);
    
    // Remove the module where we currently are.
    array_pop($module_path);
    
    while ($module_path) {
      $path = $base_path . '/' . implode('/', $module_path);
      $module_name = array_pop($module_path);
      $module_breadcrumb[] = l($module_name, $path);
    }
    //dsm($module_breadcrumb);
    if ($module_breadcrumb) {
      $breadcrumb = array_merge($breadcrumb, array_reverse($module_breadcrumb));
    }
    drupal_set_breadcrumb($breadcrumb);
  }
  
  return $output; 
}

/**
 * Render a tree of modules. Recursive function.
 *
 * @param $tree
 *  An array of module objects keyed by module machine name.
 *  The objects are of the type returned from module_rebuild_cache(),
 *  with in addition a $module->chidren property which is itself an array of
 *  module objects.
 *  To get a subtree from a particular module, simply wrap its object in an array.
 * @param $path
 *  An array of module names indicating the path down to the current module tree,
 *  if the tree is not the root. This should not include the current base module.
 */
function theme_module_hierarchy_list($tree, $path = array()) {
  //dsm($path);
  //dsm($tree);
  //dsm(implode('/', $path));
  foreach ($tree as $name => $module) {
     $items[$name] = theme('module_hierarchy_child', $module, $path);
     if (count($module->children)) {
       //dsm($name);
       //dsm(array_merge($path, array($name)));
       $items[$name] .= theme('module_hierarchy_list', $module->children, array_merge($path, array($name)));
    }
  }
  
  return theme('item_list',$items);
}

/**
 * Render a single module in the list.
 */
function theme_module_hierarchy_child($module, $path = array()) {
  $base = 'admin/build/modules/hierarchy';
  $path[] = $module->name;
  $link = $base . '/' . implode('/', $path);
  //$link = $base . '/' . $module->name;
  $output = l($module->name, $link);
  
  return $output;
}
